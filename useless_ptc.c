/* useless_ptc.c  */ 
#include <avr/io.h>
#include <avr/interrupt.h>

#include <pt.h> // protothread implementation: taken from http://dunkels.com/adam/pt/

// -- control routines
typedef enum  {DeviceON=1,DeviceOFF=0, DeviceBACKWARD=-1} DeviceCommand;
void turnRelay(DeviceCommand cmd)
{
	DDRB |= (1 << DDB2);
	DDRD |= (1 << DDD6);
	if(cmd == DeviceON){
		PORTB |= (1 << PB2);
		PORTD &= ~(1 << PD6);
		}else{
		PORTB |=(1 << PB2);
		PORTD |=(1 << PD6);
	}
}
void turnMotor(DeviceCommand cmd)
{
	DDRB |= (1 << DDB1);
	DDRD |= (1 << DDD5);
	switch(cmd)
	{
		case DeviceON:
		PORTB |= (1 << PB1);
		PORTD &= ~(1 << PD5);
		break;
		case DeviceBACKWARD:
		PORTB &= ~(1 << PB1);
		PORTD |= (1 << PD5);
		break;
		case DeviceOFF:
		PORTB |= (1 << PB1);
		PORTD |= (1 << PD5);
		break;
		default:
		break;
	}
}
void turnLED(DeviceCommand cmd)
{
	DDRD |= (1 << DDD1);
	if(DeviceON){
		PORTD |= (1 << PD1);
		}else{
		PORTD &= ~(1 << PD1);
	}
}
// -- sensor routines
int8_t isButtonPressed()
{
	DDRD &= ~(1 << DDD3); // signal in
	PORTD |= (1 << PD3); // pull up resistor activated
	return ((PIND & (1 << PD3))==0);
}
int8_t isButtonReleased()
{
	DDRD &= ~(1 << DDD2); // signal in
	PORTD |= (1 << PD2); // pull up resistor activated	
	return ((PIND & (1 << PD2))==0);
}
int8_t isArmDown()
{
	DDRD &= ~(1 << DDD7); // signal in
	PORTD |= (1 << PD7); // pull up resistor activated
	return ((PIND & (1 << PD7))==0);
}
// -- timer  routines
typedef  uint16_t u_time;
volatile u_time time=0;
u_time	schedule(u_time interval /* milliseconds */)
{ // for f=20 MHz, divider=256 and single byte timer register
	u_time delta = 625*((uint32_t)interval)/2048;	//  convert interval into timer ticks
	if ((625*((uint32_t)interval)%2048)>1024) {		// improving precision 
		delta++;
	}
	return (delta + time);
}
int8_t isExpired(u_time timer)
{
	return (time > timer);
}
// script routine
int protothread(struct pt* upt)
{  // called each timer interrupt 
	static int count=0, ix;
	static u_time timer;
	PT_BEGIN(upt); // PT_INIT is called form the main function
	// just in case to avoid race condition at the end
	PT_YIELD_UNTIL(upt,isButtonPressed());
		
	turnRelay(DeviceON); // keep device powered 
	turnMotor(DeviceOFF);
	turnLED(DeviceOFF);
	do { // counting cycle
		PT_WAIT_UNTIL(upt,isButtonReleased());
		timer = schedule(750);
		count++;
		PT_WAIT_UNTIL(upt,isExpired(timer)||
				isButtonPressed());
	} while(!isExpired(timer));
	
	for(ix = 0; ix != count; ++ix) { // working cycle
		turnLED(DeviceON);
		turnMotor(DeviceON);
		PT_WAIT_UNTIL(upt,isButtonPressed());
		turnMotor(DeviceOFF);
		timer = schedule(20);
		PT_WAIT_UNTIL(upt,isExpired(timer));
		turnMotor(DeviceBACKWARD);
		turnLED(DeviceOFF);
		PT_WAIT_UNTIL(upt,isArmDown());
		turnMotor(DeviceOFF);
		timer = schedule(20);
		PT_WAIT_UNTIL(upt,isExpired(timer));
	}
	turnRelay(DeviceOFF); // kill the machine
	timer = schedule( 20);
	PT_WAIT_UNTIL(upt,isExpired(timer));
	PT_END(upt);
	return PT_EXITED;
}
// -- interrupt routines  and main
struct pt wpt; // protothread descriptor
ISR(TIMER0_OVF_vect)
{
	time++;
	protothread(&wpt);
	return;
}
void activateTimer0()
{
	cli();
	TCCR0A=0; // Normal operation
	TCCR0B=4; // f divider 256  : ~ 3 milliseconds per interrupt
	TIMSK0 = 1;
	sei();
}
int main(void)
{
	PT_INIT(&wpt); // initiate protothread structure...
	activateTimer0();
	while(1){/*do nothing */}; 
}