# README #

### Useless Machine powered by Pololu B-168 controller ###
* This project is made as a response to the article in the issue 23 of Make Magazine. The goal is to disprove the statement that useless machine powered by controller is not real as far as it cannot turn itself off. In fact it can.

* This version of program utilize Protothread framework (http://dunkels.com/adam/pt/). Thanks to Adam Dunkels for sharing it.

* Whenever you have any questions you may write to jumbleview at gmail dot com

